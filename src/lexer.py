#!/usr/bin/env python
from lib import CallableClass, ParseError
OPS = ('>', '<', '+', '-', '*', '^', '%', '/', '!', '=', '&', '|', '~', '==',
       '!=', '>=', '<=', '&&', '||', '++', '--', '+=', '-=', '*=', '/=', '^=',
       '%=', '&=', '|=', '>>', '<<', '===', '&&=', '||=', '>>=', '<<=', '//',
       '//=', ':>', ':')
KEYWORDS = ('for', 'if', 'function', 'method', 'class', 'classmethod', 'while',
            'return', 'yield', 'break', 'continue', 'try', 'except', 'finally',
            'else', 'public', 'private', 'protected', 'raise')
VARSTART = frozenset('qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM_')
VARBODY = frozenset('1234567890qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZX'
                    'CVBNM_')
OPBODY = set(''.join(OPS))


def escape_parser(code):
    """
    * Converts code into a stream of characters.
    * Takes comments out.
    * Combines strings into single tokens.
    :param code: Code as a string.
    :return: Token stream.
    """
    in_str = False
    str_char = ''
    str_escape = False
    str_escape_block = False
    str_byte = [False, 0]
    in_comment = False
    block = ''
    for char in code.strip():
        if in_str:
            block += char
            if str_escape:
                if str_escape_block:
                    if char == '}':
                        str_escape_block = False
                        str_escape = False
                elif str_byte[0]:
                    str_byte[1] -= 1
                    if char not in '0123456789abcdef':
                        raise ParseError('invalid \\x escape')
                    if not str_byte[1]:
                        str_byte[0] = False
                        str_escape = False
                else:
                    if char == '{':
                        str_escape_block = True
                    elif char == 'x':
                        str_byte = [True, 2]
                    else:
                        str_escape = False
            else:
                if char == '\\':
                    str_escape = True
                elif char == str_char:
                    yield block
                    block = ''
                    in_str = False
        elif in_comment:
            if char == '\n':
                in_comment = False
                block = ''
        else:
            if char in ('"', "'"):
                for c in block:
                    yield c
                in_str = True
                block = char
                str_char = char
            elif char == '#':
                for c in block:
                    yield c
                in_comment = True
                block = ''
            else:
                block += char
    for c in block:
        yield c


def whitespace_parser(code):
    """
    * Concatenates character tokens not separated by whitespace.
    * Removes whitespace.
    * Leaves string tokens untouched.
    :param code: Token stream.
    :return: Token stream.
    """
    word = ''
    for token in code:
        if token[0] == token[-1] and token[0] in ("'", '"'):
            if word:
                yield word
                word = ''
            yield token
            continue
        if not token.isspace():
            word += token
        if token.isspace() and word:
            yield word
            word = ''
    if word:
        yield word


def varname_parser(code):
    """
    * Splits tokens containing both punctuation and names.
    * Splits punctuation into component parts as best as possible.
        * Semicolons are completely separated.
        * Brackets are completely separated.
        * Commas and decimal points are completely separated.
        * Operators with no spaces separating them stay together.
    :param code: Token stream.
    :return: Token stream.
    """
    for token in code:
        if token[0] == token[-1] and token[0] in ("'", '"'):
            yield token
            continue

        in_varname = False
        varname = ''

        in_op = False
        op = ''

        for c in token:
            if in_varname:
                if c in VARBODY:
                    varname += c
                else:
                    yield varname
                    if c in OPBODY:
                        in_op = True
                        op += c
                    else:
                        yield c
                    varname = ''
                    in_varname = False
            elif in_op:
                if c in OPBODY:
                    op += c
                else:
                    yield op
                    if c in VARBODY:
                        in_varname = True
                        varname += c
                    else:
                        yield c
                    op = ''
                    in_op = False
            else:
                if c in VARBODY:
                    in_varname = True
                    varname += c
                elif c in OPBODY:
                    in_op = True
                    op += c
                else:
                    yield c
        if op:
            yield op
        if varname:
            yield varname


def parse_op(token):
    """
    Subroutine of op_parser.
    :param token: Single token to parse.
    :return: Partial token stream.
    """
    if token:
        for i in (3, 2, 1):
            if token[:i] in OPS:
                yield token[:i]
                for yield_token in parse_op(token[i:]):
                    yield yield_token
                return
        raise ParseError('invalid token: {}'.format(token))


def op_parser(code):
    """
    Splits tokens consisting of multiple operators.
    :param code: Token stream.
    :return: Token stream.
    """
    for token in code:
        if all(c in OPBODY for c in token) and token not in OPS:
            for yield_token in parse_op(token):
                yield yield_token
        else:
            yield token


class lexer(metaclass=CallableClass):

    def _call(cls, code):
        """
        Convert file into flat token stream.
        :param code: Code as a string.
        :return: Token stream.
        """
        code = escape_parser(code)
        code = whitespace_parser(code)
        code = varname_parser(code)
        code = op_parser(code)
        for token in code:
            yield cls.handle_token(token)

    @staticmethod
    def handle_token(token):
        if token in OPS:
            return {'type': 'operator', 'value': token}
        elif token[0] in VARSTART and all(c in VARBODY for c in token):
            if token == 'true':
                return {'type': 'bool', 'value': True}
            elif token == 'false':
                return {'type': 'bool', 'value': False}
            elif token == 'null':
                return {'type': 'null', 'value': None}
            elif token in KEYWORDS:
                return {'type': 'keyword', 'value': token}
            else:
                return {'type': 'varname', 'value': token}
        elif token[0] == token[-1] and token[0] in ("'", '"'):
            return {'type': 'string-literal', 'value': token}
        elif token in '.()[]{};,:=':
            return {'type': 'punc', 'value': token}
        elif token.isdigit():
            return {'type': 'int-literal', 'value': token}
        elif token.startswith('0x'):
            return {'type': 'hex-literal', 'value': token[2:]}
        elif token.startswith('0b'):
            return {'type': 'binary-literal', 'value': token[2:]}
        elif token.isalnum():
            return {'type': 'alphanumeric', 'value': token}
        else:
            raise ParseError('unknown token: {}'.format(token))
