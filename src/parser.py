#!/usr/bin/env python3
import collections
import copy
import operator
from lib import ParseError
BRACKET_TYPE = {
    '{': 'curly',
    '}': 'curly',
    '[': 'square',
    ']': 'square',
    '(': 'paren',
    ')': 'paren'
}


class FilterCombinerVariable:
    # TODO: add class docstring

    def __init__(self, index, funcs=()):
        """
        :param index: The index of the window which will be substituted.
        :param funcs: Functions to apply to the value before substituting.
        """
        self.index = index
        self.funcs = funcs

    def __deepcopy__(self, memodict=None):
        """
        Shouldn't ever actually need to be copied, so it just returns a
        reference without copying.
        :param memodict:
        :return:
        """
        return self


def filter_combiner(match, save):
    """
    Creates a function that will combine tokens.
    :param match: Tuple of dictionaries with attributes to check against.
                  Example: (
                               None,
                               {'type': 'operator', 'value': '='},
                               None,
                               {'type': 'punc', 'value': ';'}
                           )
                  None indicates it doesn't matter what is in that slot.
                  If the actual tokens have extra keys, it doesn't matter.
    :param save: Dictionary indicating what token to combine the matched
                 pattern into.
                 Example: {
                              'type': 'assignment'
                              'value': (
                                  FilterCombinerVariable(0),
                                  FilterCombinerVariable(2)
                              )
                          }
    :return:
    """
    window_length = len(match)

    def matches(window):
        if len(window) < window_length:
            return False
        for i in range(window_length):
            if match[i] is None:
                continue
            for key in match[i]:
                if key not in window[i]:
                    return False
                if window[i][key] != match[i][key]:
                    return False
        return True

    def substitute_list(iterable, window):
        for i, item in enumerate(iterable):
            if isinstance(item, FilterCombinerVariable):
                subst = window[item.index]
                for func in item.funcs:
                    subst = func(subst)
                iterable[i] = subst
            elif isinstance(item, list):
                substitute_list(item, window)
            elif isinstance(item, dict):
                substitute_dict(item, window)
            elif isinstance(item, tuple):
                iterable[i] = substitute_tuple(item, window)

    def substitute_tuple(iterable, window):
        retval = []
        for item in iterable:
            if isinstance(item, FilterCombinerVariable):
                subst = window[item.index]
                for func in item.funcs:
                    subst = func(subst)
                item = subst
            elif isinstance(item, list):
                substitute_list(item, window)
            elif isinstance(item, dict):
                substitute_dict(item, window)
            elif isinstance(item, tuple):
                item = substitute_tuple(item, window)
            retval.append(item)
        return tuple(retval)

    def substitute_dict(dictionary, window):
        for key in dictionary:
            if isinstance(dictionary[key], FilterCombinerVariable):
                if dictionary[key].index >= window_length:
                    raise ValueError('Index {} is out of range for the window'.format(dictionary[key].index))
                subst = window[dictionary[key].index]
                for func in dictionary[key].funcs:
                    subst = func(subst)
                dictionary[key] = subst
            elif isinstance(dictionary[key], list):
                substitute_list(dictionary[key], window)
            elif isinstance(dictionary[key], dict):
                substitute_dict(dictionary[key], window)
            elif isinstance(dictionary[key], tuple):
                dictionary[key] = substitute_tuple(dictionary[key], window)

    def return_token(window):
        retval = copy.deepcopy(save)
        substitute_dict(retval, window)
        return retval

    def combine_func(tokens):
        token_buffer = collections.deque()
        for token in tokens:
            token_buffer.append(token)
            if len(token_buffer) > window_length:
                yield token_buffer.popleft()
            if matches(token_buffer):
                yield return_token(token_buffer)
                token_buffer.clear()
        yield from token_buffer

    return combine_func


def parse_brackets(code):
    stack = []
    for token in code:
        if token['type'] == 'punc':
            if token['value'] in '{[(':
                stack.append({
                    'type': 'brackets',
                    'bracket-type': BRACKET_TYPE[token['value']],
                    'value': []
                })
                continue
            elif token['value'] in '}])':
                if BRACKET_TYPE[token['value']] != stack[-1]['bracket-type']:
                    raise ParseError('unexpected bracket: {}'.format(token['value']))
                if len(stack) == 1:
                    yield stack.pop()
                else:
                    stack[-2]['value'].append(stack.pop())
                continue
        if len(stack) == 0:
            yield token
        else:
            stack[-1]['value'].append(token)


def combine_variables(tokens):
    temp = []
    for token in tokens:
        if not temp:
            if token['type'] == 'varname':
                temp.append(token)
            else:
                yield token
        elif temp[-1]['type'] == 'punc' and temp[-1]['value'] == '.':
            if token['type'] == 'varname':
                temp.append(token)
            else:
                raise ParseError('invalid syntax: {}.{}'.format(temp[-2]['value'], token['value']))
        elif temp[-1]['type'] == 'varname':
            if token['type'] == 'punc' and token['value'] == '.':
                temp.append(token)
            else:
                yield {'type': 'variable', 'value': tuple(t['value'] for t in temp[::2])}
                temp = []
                yield token
        else:
            raise Exception
    if temp:
        if temp[-1]['type'] == 'punc' and temp[-1]['value'] == '.':
            raise ParseError
        elif temp[-1]['type'] == 'varname':
            yield {'type': 'variable', 'value': tuple(t['value'] for t in temp[::2])}
        else:
            raise Exception


def combine_float(tokens):
    temp = []
    for token in tokens:
        if temp:
            if temp[-1]['type'] == 'punc':
                to_yield_token = False
                acceptable = ('int-literal',)
                if temp[0]['type'] == 'hex-literal':
                    acceptable += ('varname', 'alphanumeric')
                if token['type'] in acceptable:
                    temp.append(token)
                else:
                    to_yield_token = True
                yield {
                    'type': 'float-literal',
                    'value': ''.join(tok['value'] for tok in temp),
                    'base': {
                        'int-literal': 10,
                        'binary-literal': 2,
                        'hex-literal': 16
                    }[temp[0]['type']]
                }
                if to_yield_token:
                    yield token
                temp = []
            else:
                if token['type'] == 'punc' and token['value'] == '.':
                    temp.append(token)
                else:
                    yield temp[0]
                    yield token
                    temp = []
        else:
            if token['type'] in ('int-literal', 'hex-literal', 'binary-literal'):
                temp.append(token)
            else:
                yield token
    if len(temp) == 1:
        yield from temp
    elif len(temp) == 2:
        yield {
            'type': 'float-literal',
            'value': ''.join(tok['value'] for tok in temp),
            'base': {
                'int-literal': 10,
                'binary-literal': 2,
                'hex-literal': 16
            }[temp[0]['type']]
        }


# def combine_brace_access(tokens):
#     tokens = tuple(tokens)  # TODO: delete since this is only for debugging
#     operand = None
#     for token in tokens:
#         if operand is None:
#             if token['type'] == 'keyword' or token['type'] == 'operator':
#                 yield token
#             else:
#                 operand = token
#         else:
#             if token['type'] == 'brackets':
#
#                 # handle function call
#                 if token['bracket-type'] == 'paren':
#                     if operand['type'] == 'brackets':
#                         if operand['bracket-type'] == 'paren':
#                             operand = combine_expressions(operand['value'])
#                         else:
#                             # TODO: []() should be runtime error array not callable
#                             raise ParseError('unexpected function call on {} '
#                                              'brackets'.format(
#                                 operand['bracket-type'])
#                             )
#                     operand = {
#                         'type': 'function-call',
#                         'value': operand,
#                         'arguments': combine_expressions(token['value']),
#                         'expression': True  # do this for other expressions
#                     }
#                     continue
#
#                 # handle indexing/dict access
#                 elif token['bracket-type'] == 'square':
#                     if operand['type'] == 'brackets':
#                         if operand['bracket-type'] == 'paren':
#                             operand = combine_expressions(operand['value'])
#                         else:
#                             raise ParseError('unexpected access on {} '
#                                              'brackets'.format(
#                                 operand['bracket-type'])
#                             )
#                     operand = {
#                         'type': 'access',
#                         'value': operand,
#                         'arguments': combine_expressions(token['value'])
#                     }
#                     continue
#
#             yield operand
#             if token['type'] == 'keyword':
#                 yield token
#                 operand = None
#             else:
#                 operand = token
#     if operand:
#         yield operand


def generate_operation(token_buffer, precedence):
    token_buffer = tuple(token_buffer)
    if precedence == 'right':
        operands = [None, None]
        operands[0] = token_buffer[0]
        if len(token_buffer) == 3:
            operands[1] = token_buffer[2]
        else:
            operands[1] = generate_operation(token_buffer[2:], 'left')
        for operand in operands:
            # TODO: make sure this is right
            if operand['type'] == 'brackets':
                operand['value'] = combine_expressions(operand['value'])
        return {
            'type': 'operation',
            'value': token_buffer[1]['value'],
            'operands': tuple(operands)
        }
    elif precedence == 'left':
        operands = [None, None]
        operands[1] = token_buffer[-1]
        if len(token_buffer) == 3:
            operands[0] = token_buffer[0]
        else:
            operands[0] = generate_operation(token_buffer[:-2], 'right')
        for i, operand in enumerate(operands):
            operands[i] = reduce_parens(operand)
        return {
            'type': 'operation',
            'value': token_buffer[1]['value'],
            'operands': tuple(operands)
        }
    elif precedence == 'chained':
        if len(token_buffer) == 3:
            return {
                'type': 'operation',
                'value': token_buffer[1]['value'],
                'operands': (token_buffer[0], token_buffer[2])
            }
        else:
            return {
                'type': 'operation',
                'value': '&&',
                'operands': (generate_operation(token_buffer[:3], 'chained'),
                             generate_operation(token_buffer[2:], 'chained'))
            }
    else:
        raise RuntimeError('unknown precedence: {}'.format(precedence))


# TODO: check this functionality is not already in combine bracketed literals
#       and just isn't being applied in the right order
def reduce_parens(token):
    """
    Returns token, unless token is parentheses, in which case it calls
    combine_expressions on the contents, raises a ParseError if more than one
    token remains, and then returns that one remaining token.
    :param token:
    :return:
    """
    if token['type'] == 'brackets' and token['bracket-type'] == 'paren':
        tokens = combine_expressions(token['value'])
        try:
            token, = tokens
        except ValueError:
            # combine_expressions returned multiple tokens
            raise ParseError(
                'invalid expression in parens as operand to binary op'
            )
    return token


def combine_binary_operators(tokens, operators, precedence):
    token_buffer = collections.deque()
    in_operation = False
    for token in tokens:

        if len(token_buffer) < 1:
            token_buffer.append(token)
            continue

        if in_operation:
            if token_buffer[-1]['type'] != 'operator':
                if token['type'] == 'operator' and token['value'] in operators:
                    token_buffer.append(token)
                else:
                    yield generate_operation(token_buffer, precedence)
                    token_buffer.clear()
                    token_buffer.append(token)
                    in_operation = False
            else:
                token_buffer.append(token)
        else:
            token_buffer.append(token)
            if token['type'] == 'operator' and token['value'] in operators:
                in_operation = True
            else:
                yield token_buffer.popleft()

    if in_operation:
        if token_buffer[-1]['type'] == 'operator':
            raise ParseError('binary operator {} with no second operand'.format(token_buffer[-1]['value']))
        yield generate_operation(token_buffer, precedence)
    else:
        yield from token_buffer


def is_valid_operand(token):
    # TODO: this is probably incomplete
    return token['type'] in {'variable', 'brackets', 'int-literal'}


def combine_unary_operators(tokens, operators):
    in_op = False
    prev_token = None
    op_buffer = []
    for token in tokens:
        is_op = (
            token['type'] == 'operator'
            and token['value'] in operators
            and (prev_token is None or not is_valid_operand(prev_token))
        )
        if is_op:
            op_buffer.append(token['value'])
            in_op = True
        if in_op:
            if not is_op:
                if not is_valid_operand(token):
                    raise ParseError('invalid operand for unary operator: {}'.format(token))

                op_token = reduce_parens(token)

                # Layer the unary operators right to left
                for op in op_buffer[::-1]:
                    op_token = {
                        'type': 'operation',
                        'value': op,
                        'operands': (op_token,)
                    }

                yield prev_token
                prev_token = None
                yield op_token
                in_op = False
                op_buffer = []
        else:
            if prev_token is not None:
                yield prev_token
            prev_token = token
    if in_op:
        raise ParseError  # TODO: ns traceback
    if prev_token is not None:
        yield prev_token


def combine_commas(tokens):
    separators = ({'type': 'operator', 'value': '='},
                  {'type': 'punc', 'value': ';'})
    comma_list = []
    token_buffer = []
    for token in tokens:
        if token in separators + ({'type': 'punc', 'value': ','},):
            comma_list.append(tuple(combine_expressions(token_buffer)))
            token_buffer = []
            if token in separators:
                if len(comma_list) > 1:
                    yield {'type': 'comma-list', 'value': tuple(comma_list)}
                elif len(comma_list) == 1:
                    yield from comma_list[0]
                comma_list = []
                yield token
            continue
        token_buffer.append(token)
    if comma_list:
        if token_buffer:
            comma_list.append(tuple(combine_expressions(token_buffer)))
            token_buffer = []
        yield {'type': 'comma-list', 'value': tuple(comma_list)}
    if token_buffer:
        yield from token_buffer


def combine_bracketed_literals(tokens):
    """
    Interprets literals that involve brackets
    :param tokens:
    :return: tokens
    """
    for token in tokens:
        if token['type'] == 'brackets':
            contents = combine_expressions(token['value'])
            if token['bracket-type'] == 'square':
                vals = []
                if len(contents) == 1:
                    if contents[0]['type'] == 'comma-list':
                        vals.extend(contents[0]['value'])
                    else:
                        vals.append(contents[0]['value'])
                elif len(contents) > 1:
                    # TODO: rebuild code from lexed code and print it
                    raise ParseError
                yield {
                    'type': 'list-literal',
                    'value': tuple(vals)
                }
            elif token['bracket-type'] == 'paren':
                if len(contents) == 1:
                    if contents[0]['type'] == 'comma-list':
                        yield {
                            'type': 'tuple-literal',
                            'value': tuple(contents[0]['value'])
                        }
                    else:
                        # Remove redundant parens
                        yield contents[0]
                elif len(contents) > 1:
                    # TODO: rebuild code from lexed code and print it
                    raise ParseError
            elif token['bracket-type'] == 'curly':
                if len(contents) == 1:
                    if contents[0]['type'] == 'comma-list':
                        # set or dict literal
                        if (
                                    len(contents[0]['value'][0]) == 3
                                    and contents[0]['value'][0][1] == {
                                        'type': 'operator',
                                        'value': ':'
                                    }
                                ):
                            # dict literal
                            vals = []
                            for item in contents[0]['value']:
                                if len(item) == 3 and item[1] == {
                                            'type': 'operator',
                                            'value': ':'
                                        }:
                                    vals.append((item[0], item[2]))
                                else:
                                    raise ParseError('invalid dict literal')
                            yield {
                                'type': 'dict-literal',
                                'value': tuple(vals)
                            }
                        elif len(contents[0]['value'][0]) == 1:
                            # set literal
                            vals = []
                            for item in contents[0]['value']:
                                if len(item) == 0:
                                    vals.append(item[0])
                                else:
                                    raise ParseError('invalid set literal')
                            yield {
                                'type': 'set-literal',
                                'value': tuple(vals)
                            }
                        else:
                            # TODO: rebuild code from lexed code and print it
                            raise ParseError

                    else:
                        # 1-item set literal
                        yield {
                            'type': 'set-literal',
                            'value': (contents[0],)
                        }
                elif not len(contents):
                    # empty dict literal
                    yield {
                        'type': 'dict-literal',
                        'value': ()
                    }
                elif len(contents) == 3 and contents[1] == {
                            'type': 'operator',
                            'value': ':'
                        }:
                    # 1-item dict literal
                    yield {
                        'type': 'dict-literal',
                        'value': ((contents[0], contents[2]),)
                    }
                    continue
                elif len(contents) > 1:
                    # code block
                    yield {
                        'type': 'code-block',
                        'value': tuple(contents)
                    }
        else:
            yield token


# TODO: parse class(){}
# TODO: fix that function(){} parses {} as dict-literal not code-block
def combine_expressions(tokens):
    # all 'brackets' type tokens should be gone by the end of this
    tokens = combine_variables(tokens)
    tokens = combine_float(tokens)

    # combine function() {}
    tokens = filter_combiner(
        (
            {'type': 'keyword', 'value': 'function'},
            {'type': 'brackets', 'bracket-type': 'paren'},
            {'type': 'brackets', 'bracket-type': 'curly'}
        ),
        {
            'type': 'function',
            'arguments': FilterCombinerVariable(1, funcs=(
                operator.itemgetter('value'),
                combine_expressions
            )),
            'value': FilterCombinerVariable(2, funcs=(
                operator.itemgetter('value'),
                combine_expressions
            ))
        }
    )(tokens)

    # combine class() {}
    tokens = filter_combiner(
        (
            {'type': 'keyword', 'value': 'class'},
            {'type': 'brackets', 'bracket-type': 'paren'},
            {'type': 'brackets', 'bracket-type': 'curly'}
        ),
        {
            'type': 'class',
            'parents': FilterCombinerVariable(1, funcs=(
                operator.itemgetter('value'),
                combine_expressions
            )),
            'value': FilterCombinerVariable(2, funcs=(
                operator.itemgetter('value'),
                combine_expressions
            ))
        }
    )(tokens)

    tokens = combine_commas(tokens)
    # tokens = combine_brace_access(tokens)

    # combine function calls
    tokens = filter_combiner(
        (
            {'type': 'variable'},
            {'type': 'brackets', 'bracket-type': 'paren'}
        ),
        {
            'type': 'function-call',
            'value': FilterCombinerVariable(0),
            'arguments': FilterCombinerVariable(1, funcs=(
                operator.itemgetter('value'),
                combine_expressions
            )),
            # 'expression': True  # do this for other expressions
        }
    )(tokens)
    # TODO: combine brace[access]

    tokens = combine_unary_operators(tokens, ('!', '-'))
    tokens = combine_binary_operators(tokens, ('^',), 'right')
    tokens = combine_binary_operators(tokens, ('*', '/', '//'), 'left')
    tokens = combine_binary_operators(tokens, ('+', '-'), 'left')
    tokens = combine_binary_operators(tokens, ('==', '!=', '<', '>', '<=',
                                               '>=', ':>'), 'chained')
    tokens = combine_binary_operators(tokens, ('===',), 'chained')
    tokens = combine_binary_operators(tokens, ('&&', '||'), 'left')
    tokens = combine_bracketed_literals(tokens)
    return tuple(tokens)


def combine_body_statements(tokens):
    for token in tokens:
        if token['type'] == 'function' or token['type'] == 'class':
            token['value'] = combine_statements(token['value'])
            if token['type'] == 'class':
                token['value'] = combine_class_statements(token['value'])
        yield token


def combine_class_statements(tokens):
    """
    Handle syntax specific to classes like `public`, `private`, `method`, etc.
    :param tokens:
    :return: tokens
    """

    combine_visibility_assignment = lambda visibility: filter_combiner(
        (
            {'type': 'keyword', 'value': visibility},
            {'type': 'assignment', 'visibility': None}
        ),
        {
            'type': 'assignment',
            'value': FilterCombinerVariable(1, funcs=(
                operator.itemgetter('value'),
            )),
            'visibility': visibility
        }
    )
    tokens = combine_visibility_assignment('public')(tokens)
    tokens = combine_visibility_assignment('private')(tokens)
    tokens = combine_visibility_assignment('protected')(tokens)

    # combine assignment
    def process_method_name(token):
        if token['value']['type'] != 'variable':
            raise ParseError
        return token['value']['value']
    tokens = filter_combiner(
        (
            {'type': 'keyword', 'value': 'method'},
            {'type': 'function-call'},
            {'type': 'code-block'}
        ),
        {
            'type': 'method',
            'value': FilterCombinerVariable(2, funcs=(
                operator.itemgetter('value'),
            )),
            'name': FilterCombinerVariable(1, funcs=(process_method_name,)),
            'visibility': None
        }
    )(tokens)

    return tuple(tokens)


def combine_statements(tokens):
    """
    :param tokens:
    :return: tokens
    """
    tokens = combine_body_statements(tokens)

    # combine assignment
    tokens = filter_combiner(
        (
            None,
            {'type': 'operator', 'value': '='},
            None,
            {'type': 'punc', 'value': ';'}
        ),
        {
            'type': 'assignment',
            'value': (FilterCombinerVariable(0), FilterCombinerVariable(2)),
            'visibility': None
        }
    )(tokens)

    # combine for loops
    tokens = filter_combiner(
        (
            {'type': 'keyword', 'value': 'for'},
            {'type': 'operation', 'value': ':>'},
            {'type': 'code-block'}
        ),
        {
            'type': 'for-loop',
            'value': FilterCombinerVariable(2, funcs=(
                operator.itemgetter('value'),
                combine_statements
            )),
            'iterable': FilterCombinerVariable(1, funcs=(
                operator.itemgetter('operands'),
                operator.itemgetter(1)
            )),
            'assignee': FilterCombinerVariable(1, funcs=(
                operator.itemgetter('operands'),
                operator.itemgetter(0)
            ))
        }
    )(tokens)

    # combine expression statements
    tokens = filter_combiner(
        (
            {'expression': True},
            {'type': 'punc', 'value': ';'}
        ),
        {
            'type': 'evaluate',
            'value': FilterCombinerVariable(0)
        }
    )(tokens)

    return tuple(tokens)
