#!/usr/bin/env python3
import abc
import types
from data_model import ns_builtin_namespace as builtin_namespace


class ASTException(Exception):
    """
    Generic exception for AST.
    """


class ASTNode(object):
    """
    Abstract syntax tree node.
    """


class Executable(ASTNode):
    __metaclass__ = abc.ABCMeta

    @abc.abstractmethod
    def run(self, namespace):
        return NotImplemented


class Expression(ASTNode):
    __metaclass__ = abc.ABCMeta

    @abc.abstractmethod
    def evaluate(self, namespace):
        return NotImplemented


class Module(Executable):

    def __init__(self, name):
        """
        :type name: str
        """
        self.name = name
        self.code = []

    def add_code(self, code):
        self.code.append(code)

    def run(self, namespace=None):
        for node in self.code:
            if isinstance(node, Executable):
                node.run(builtin_namespace.add_namespace('module'))
            elif isinstance(node, Expression):
                node.evaluate(builtin_namespace.add_namespace('module'))


class Class(Expression):

    def __init__(self, inherits_from):
        """
        :type inherits_from: tuple
        """
        self.inherits_from = inherits_from
        self.code = []

    def add_code(self, code):
        self.code.append(code)

    def evaluate(self, namespace):
        """
        :type namespace: NamespaceStack
        """
        pass


class Evaluate(Executable):
    """
    This Executable evaluates an Expression and does nothing with it.
    """

    def __init__(self, expression):
        if not isinstance(expression, Expression):
            raise ValueError("'{}' is not an Expression".format(expression))
        self.expression = expression

    def run(self, namespace):
        self.expression.evaluate(namespace)


class Call(Expression):

    def __init__(self, ns_callable, args):
        self.callable = ns_callable
        if not all(isinstance(arg, Expression) for arg in args):
            raise ValueError
        self.args = args

    def evaluate(self, namespace):
        if isinstance(self.callable, Callable):
            ns_callable = self.callable
        elif isinstance(self.callable, Variable):
            ns_callable = self.callable.get(namespace)
        else:
            raise ASTException('unknown callable type in Call: {}'.format(
                type(self.callable)
            ))

        ns_callable.run([arg.evaluate(namespace) for arg in self.args])


class Callable(Expression):

    def __init__(self, callable_type, args, bind=None):
        """
        :type callable_type: 'function', 'method',
        """
        if callable_type == 'method':
            if bind is None:
                raise TypeError('Must provide bind to method')

        self.callable_type = callable_type
        self.bind = bind
        self.args = args
        self.code = []

    def add_code(self, code):
        self.code.append(code)

    def evaluate(self, namespace):
        """
        :type namespace: NamespaceStack
        """
        ns_obj = builtin_namespace.evaluate_variable('__internal__').getattr(
            'callable'
        ).instantiate(namespace)
        if self.bind is not None:
            ns_obj.setattr('__bind__', self.bind.evaluate(namespace))

        def run(self2, args):
            function_namespace = namespace.add_namespace('callable')
            try:
                args = (self2.getattr('__bind__'),) + args
            except AttributeError:
                pass
            for i, arg in enumerate(self.args):
                function_namespace.assign_variable(arg, args[i])
                # TODO: check if number of args is correct
            for node in self.code:
                if isinstance(node, Executable):
                    node.run(function_namespace)
                elif isinstance(node, Expression):
                    node.evaluate(function_namespace)

        ns_obj.run = types.MethodType(run, ns_obj)
        return ns_obj


class Variable(ASTNode):

    def __init__(self, name):
        """
        :param name: ideally a tuple of str
        """
        self.name = tuple(name)

    def set(self, value, namespace):
        """
        :param value:
        :type value: data_model.Object
        :param namespace:
        :type namespace: data_model.NamespaceStack
        """
        if len(self.name) == 1:
            pass
        else:
            base = namespace.evaluate_variable(self.name[0])
            for name_part in self.name[1:-1]:
                base = base.getattr(name_part)
            base.setattr(self.name[-1], value)

    def get(self, namespace):
        """
        :param namespace:
        :type namespace: data_model.NamespaceStack
        """
        base = namespace.evaluate_variable(self.name[0])
        for name_part in self.name[1:]:
            base = base.getattr(name_part)
        return base


class Assignment(Executable):

    def __init__(self, variable):
        self.variable = variable
        self.value = None

    def set_value(self, value):
        """
        :param value:
        :type value: ASTNode
        """
        self.value = value

    def run(self, namespace):
        """
        :type namespace: data_model.NamespaceStack
        """
        if self.value is None:
            raise ASTException('must set value before running Assignment')
        if type(self.variable) == Variable:
            self.variable.set(self.value.evaluate(namespace), namespace)
            namespace.assign_variable(self.variable, self.value.evaluate(namespace))
        else:
            # TODO: assign to index access
            pass


def _verify_executable_list_type(exec_list):
    for item in exec_list:
        if not isinstance(item, Executable):
            raise TypeError


class Conditional(Executable):

    def __init__(self, condition, on_true, on_false=()):
        if not isinstance(condition, Expression):
            raise TypeError
        self.condition = condition
        self.on_true = tuple(on_true)
        self.on_false = tuple(on_false)
        _verify_executable_list_type(self.on_true)
        _verify_executable_list_type(self.on_false)

    def run(self, namespace):
        condition = self.condition.evaluate(namespace)
        # TODO: deal with truthy and falsy values
        raise NotImplementedError
        # if condition:
        #     for statement in self.on_true:
        #         statement.run(namespace)
        # else:
        #     for statement in self.on_false:
        #         statement.run(namespace)


class IntLiteral(Expression):

    def __init__(self, value):
        self.value = value

    def evaluate(self, namespace):
        return builtin_namespace.evaluate_variable('int').instantiate(
            self.value,
            namespace
        )


class StringLiteral(Expression):

    @staticmethod
    def parse_string_literal(s):
        # quote = s[0]
        return eval(s)  # TODO: CHANGE THIS

    def __init__(self, value):
        self.value = self.parse_string_literal(value)

    def evaluate(self, namespace):
        return builtin_namespace.evaluate_variable('str').instantiate(
            self.value,
            namespace
        )
