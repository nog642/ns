#!/usr/bin/env python3


class CallableClass(type):

    def __new__(mcs, name, bases, namespace):
        if '_call' not in namespace:
            raise Exception('CallableClass must define _call()')

        namespace['__call__'] = classmethod(namespace['_call'])
        cls = super().__new__(mcs, name, bases, namespace)
        type.__init__(cls, name, bases, namespace)
        return cls()


class ParseError(Exception):
    pass
