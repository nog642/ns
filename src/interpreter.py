#!/usr/bin/env python3
import os
import AST
from lexer import lexer
from lib import ParseError
from src.parser import parse_brackets, combine_expressions, combine_statements


class Interpreter:

    def __init__(self, code):
        self.ast = AST.Module('__main__')
        tokens = lexer(code)
        tokens = parse_brackets(tokens)
        tokens = combine_expressions(tokens)
        tokens = combine_statements(tokens)

        self.tokens = tokens

    @staticmethod
    def parse_expression(token):
        if token['type'] == 'function':

            # parse arguments to function
            if len(token['arguments']) > 1:
                # TODO: add default arguments
                raise ParseError
            if len(token['arguments']) == 0:
                args = ()
            elif token['arguments'][0]['type'] == 'comma-list':
                args = []
                for arg in token['arguments'][0]['value']:
                    if len(arg) == 1:
                        arg = arg[0]
                    else:
                        raise ParseError
                    if arg['type'] == 'variable':
                        if len(arg['value']) > 1:
                            # TODO: recreate code and print it
                            raise ParseError('Invalid syntax')
                        args.append(arg['value'][0])
                args = tuple(args)
            elif token['arguments'][0]['type'] == 'variable':
                if len(token['arguments'][0]['value']) > 1:
                    # TODO: recreate code and print it
                    # this is the same as the one above, so maybe move code
                    # recreation to a separate function
                    raise ParseError('Invalid syntax')
                args = (token['arguments'][0]['value'][0],)
            else:
                raise ParseError

            node = AST.Callable('function', args)
            for j_token in token['value']:
                node.add_code(Interpreter.parse_statement(j_token))
            return node

        elif token['type'] == 'function-call':
            if token['value']['type'] == 'variable':
                ns_callable = AST.Variable(token['value']['value'])
            elif token['value']['type'] == 'string-literal':
                print('skipping string formatting')
                return
            else:
                raise ParseError('attempted function call on token type: {}'.format(token['value']['type']))

            # parse arguments to function
            if len(token['arguments']) > 1:
                # TODO: add named arguments
                raise ParseError
            if len(token['arguments']) == 0:
                args = ()
            elif token['arguments'][0]['type'] == 'comma-list':
                args = []
                for arg in token['arguments'][0]['value']:
                    if len(arg) == 1:
                        arg = arg[0]
                    else:
                        raise ParseError
                    args.append(Interpreter.parse_expression(arg))
                args = tuple(args)
            else:
                args = (Interpreter.parse_expression(token['arguments'][0]),)

            return AST.Call(ns_callable, args)
        elif token['type'] == 'int-literal':
            return AST.IntLiteral(token['value'])
        elif token['type'] == 'string-literal':
            return AST.StringLiteral(token['value'])
        else:
            # TODO: add more
            print('unparsed expression')

    @staticmethod
    def parse_statement(token):
        if token['type'] == 'assignment':
            if token['value'][0]['type'] == 'variable':
                node = AST.Assignment(AST.Variable(token['value'][0]['value']))
            else:
                raise ParseError
            node.set_value(Interpreter.parse_expression(token['value'][1]))
            return node
        elif token['type'] == 'evaluate':
            return AST.Evaluate(Interpreter.parse_expression(token['value']))
        else:
            print('unparsed token')
            # TODO: raise an error after all expected tokens are handled

    def generate_AST(self):
        for token in self.tokens:
            self.ast.add_code(Interpreter.parse_statement(token))


def main():
    from argparse import ArgumentParser

    argument_parser = ArgumentParser()
    argument_parser.add_argument('ns_file')
    args = argument_parser.parse_args()

    if not os.path.isfile(args.ns_file):
        raise ValueError("'{}' is not a file".format(args.ns_file))

    with open(args.ns_file) as f:
        code = f.read()

    interpreter = Interpreter(code)
    interpreter.generate_AST()
    interpreter.ast.run()


# def testmain():
#     with open('../test_example.ns', 'r') as f:
#
#         # from lexer import varname_parser, op_parser
#         # x = list(varname_parser(f.read()))
#         # x = list(op_parser(f.read()))
#
#         # x = list(Lexer().get_tokens(f.read()))
#         # x = list(parse_brackets(f.read()))
#         # x = combine_expressions(parse_brackets(f.read()))
#         x = Parser(f.read()).tokens
#
#         # x = Parser(f.read())
#         # x.generate_AST()
#         # x = x.get_AST()
#
#         print(x)


if __name__ == '__main__':
    main()
