#!/usr/bin/env python3
import types

MODULE_STRUCTURE = {
    '__internal__':  '__internal__',
    'class':         '__internal__.class',
    'callable':      '__internal__.callable',
    'module':        '__internal__.module',
    'int':           'int',
    'str':           'str',
    'console':       'console',
    'console.write': 'console.write',
    'math':          'math',
    'abs':           'math.abs',
    'linalg':        'math.linalg',  # submodule
    'null':          'null',
    'object':        'object',
    'tools':         'tools',
    'abc':           'tools.abc',
    'abc.required':  'tools.abc.required'
}


ns_builtins = {}


class NamespaceStack:

    def __init__(self):
        self.types = ['builtin']
        self.vars = [ns_builtins]

    def add_namespace(self, namespace_type):
        """
        Return a new NamespaceStack (copy of self) with an empty namespace
        added to it of the specified type.
        :param namespace_type:
        :type namespace_type: str
        :return:
        :rtype: NamespaceStack
        """
        other = NamespaceStack()
        other.types = self.types.copy()

        # This is intentionally shallow copy so that dicts inside of vars stay
        # the same objects
        other.vars = self.vars.copy()

        if namespace_type in ('module', 'callable', 'class'):
            other.types.append(namespace_type)
            other.vars.append({})
        elif namespace_type in ('loop', 'try', 'except', 'try-else'):
            other.types.append(namespace_type)
            other.vars.append(None)
        else:
            raise ValueError("Namespace type must be 'module', 'callable', "
                             "'class', 'loop', 'try', 'except', or 'try-else',"
                             " not '{}'".format(namespace_type))

        return other

    def assign_variable(self, name, value):
        """
        Assigns a variable to the highest available namespace.
        :param name:
        :type name: str
        :param value:
        :type value: Object
        """
        # TODO: assigning to outer scope is impossible in a function with this
        for variables in self.vars[::-1]:
            if variables is not None:
                variables[name] = value
                break

    def evaluate_variable(self, name):
        """
        Gets a variable from the highest namespace where it is available.
        :param name:
        :type name: str
        :return:
        :rtype: Object
        """
        for variables in self.vars[::-1]:
            if variables is None:
                continue
            if name in variables:
                return variables[name]
        # TODO: raise an ns exception here


ns_builtin_namespace = NamespaceStack()


class Object:

    def __init__(self):
        self._attributes = {
            '__name__': 'object',
            '__class__': self,
            '__bases__': ()
        }
        self.instantiable = True

    def __repr__(self):
        return 'NSobject:{}-{}'.format(self._attributes['__name__'], hash(self))

    def setattr(self, name, value):
        self._attributes[name] = value

    def getattr(self, name):
        if name == '__bases__':
            return self._attributes['__bases__']
            # TODO: return ns type instead of python list
        if name == '__call__':
            if self._attributes['__class__'] == ns_builtins['__internal__'].getattr('callable'):
                # TODO: handle inheritance
                return self
            else:
                raise NotImplementedError
        for class_to_search in (self,) + self._attributes['__bases__'] + (self._attributes['__class__'],):
            # TODO: maybe eliminate recursiveness to improve performance, as
            #       current algorithm repeats operations
            try:
                if class_to_search is self:
                    if name in self._attributes:
                        return self._attributes[name]
                else:
                    return class_to_search.getattr(name)
            except AttributeError:
                pass
        raise AttributeError  # TODO: raise an ns exception

    def instantiate(self, namespace, parents=()):
        if not self.instantiable:
            raise Exception('tried to instantiate uninstantiable object')  # TODO: raise ns exception
        if not parents:
            parents = (ns_builtins['object'],)
        instance = Object()
        instance.setattr('__name__', self._attributes['__name__'] + '(instance)')
        instance._attributes['__bases__'] = parents
        instance.setattr('__class__', self)
        if '__init__' in self._attributes:
            self._attributes['__init__'].run(namespace, args=(self,))
            # TODO: consider going through __run__ lest __init__ not be an ns object
        return instance


def get_builtin_from_location(location):
    """
    :param location: Location of ns object in module structure as list.
    :return: Builtin ns object.
    """
    base = ns_builtins[location[0]]
    for module_name in location[1:]:
        base = base.getattr(module_name)
    return base


def get_builtin(name):
    """
    :param name: Location of ns object in module structure as string.
    :return: Builtin ns object.
    """
    return get_builtin_from_location(MODULE_STRUCTURE[name].split('.'))


def assign_builtin(name, value):
    location = MODULE_STRUCTURE[name].split('.')
    if len(location) == 1:
        ns_builtins[name] = value
        return
    base = get_builtin_from_location(location[:-1])
    base.setattr(name, value)


def setattr_builtin(name, attr_name, value):
    get_builtin(name).setattr(attr_name, value)


# object
assign_builtin('object', Object())


# null
assign_builtin('null', ns_builtins['object'].instantiate(ns_builtin_namespace))
# TODO: make null a keyword instead


# __internal__.module
ns_module = ns_builtins['object'].instantiate(ns_builtin_namespace)


# __internal__
assign_builtin('__internal__', ns_module.instantiate(ns_builtin_namespace))


# __internal__.module
assign_builtin('module', ns_module)
del ns_module


# __internal__.callable
assign_builtin('callable', ns_builtins['object'].instantiate(ns_builtin_namespace))


# __internal__.class
assign_builtin('class', ns_builtins['object'].instantiate(ns_builtin_namespace))


# int
assign_builtin('int', get_builtin('class').instantiate(ns_builtin_namespace))
get_builtin('int').value = None
super_instantiate = get_builtin('int').instantiate
# TODO: standardize construction args
def instantiate(value, namespace, parents=()):
    instance = super_instantiate(namespace, parents)
    instance.instantiable = False
    instance.value = value
    return instance
get_builtin('int').instantiate = instantiate


# str
assign_builtin('str', get_builtin('class').instantiate(ns_builtin_namespace))
get_builtin('str').value = None
super_instantiate = get_builtin('str').instantiate
# TODO: standardize construction args
def instantiate(value, namespace, parents=()):
    instance = super_instantiate(namespace, parents)
    instance.instantiable = False
    instance.value = value
    return instance
get_builtin('str').instantiate = instantiate


# console
assign_builtin('console', get_builtin('module').instantiate(ns_builtin_namespace))


# console.write
console_write_func = get_builtin('callable').instantiate(ns_builtin_namespace)
def run(self, args):
    if len(args) > 1:
        raise Exception
        # TODO: do something else
    if args[0].getattr('__class__') is get_builtin('str'):
        print(args[0].value)
    elif args[0].getattr('__class__') is get_builtin('int'):
        print(args[0].value)
console_write_func.run = types.MethodType(run, console_write_func)
setattr_builtin(
    'console',
    'write',
    console_write_func
)


# TODO: uncomment this once all objects listed in MODULE_STRUCTURE have actually been created
# for built_in in MODULE_STRUCTURE:
#     setattr_builtin(built_in, '__name__', MODULE_STRUCTURE[built_in])


if __name__ == '__main__':
    print(ns_builtins)
