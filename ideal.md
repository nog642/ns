Declarations are like Python except they have semicolons.

```
x = 5;
y = 'foo';
```

Declarations without equal signs set the value to `null`.

```
x;
```

More than 2 underscores in a row is not allowed for any variable names, as those are reserved for the compiler.

---

Integer literals are like Python 3.

```
x = 5;
y = 230529359021040235235;
```

---

Boolean literals are like JS.

```
x = true;
y = false;
```

---

`null` has the same name as in JS or Java and functions like `None` in Python.

```
x = null;
```

---

String literals will use UTF-8 like Python 3. They will also use `\n`, `\r`, `\t`, `\'`, `\\`, etc escape characters like Python.

```
x = 'Hello World!\r\n';
y = "d𐍈n't";
```

Escape characters can also have curly braces (`\{n}`, `\{r}`, `\{t}`), which allows for unicode characters in a single excape.

Byte entry will be allowed, but UTF-8 will not be processed on them, just like Python 3.

```
z = "d\xF0\x90\x8D\x88n't";
y = "d\{U10348}n't";
```

Old idea:

>>>
Strings take input similar to the `format` method in Python, except instead of `{}`, the format character is the empty escape `\{}`. The syntax looks like you are calling the string.

```
for i in range(10) {
    console.write('i = \{}'(i));
}
```

The `format` mini-language from Python will also be implemented somehow, probably in a modified form.
>>>

New idea:

>>>
Maybe something closer to Python f-strings, or JS template strings. Maybe `\${expr}` to substitute an expression. `\{${expr}}` seems clunky, but for consistency maybe it should be valid syntax.
>>>

---

Unary operators will include `-`, `~`, and `!`.

`!!x` and `~~x` will be valid chaining, but `--x` will not.

---

Printing is handled by the `console` object.

`console.write` is like Python 3's `print`.

```
console.write(5);
console.write('Hello World!');
console.write('Processing...', end='');
```

`console.read` is like Python 3's `input`.

```
x = console.read();
y = console.read('Your response: ');
```

---

If statements are like Python in that they do not require parens around the condition. Otherwise, they are like JS.

```
if x < 5 {
    console.write("x < 5");
} else if x > 5 {
    console.write("x > 5");
} else {
    console.write("x = 5");
}
```

---

While loops are basically the same as if statements.

```
while i < 10 {
    i++;
    console.write(i);
}
```

While loops may also have `break` and `continue` statements, as well as `else` clauses that work like for-else loops in Python.

```
while i < 100 {
    i++;
    if i % 10 == 0 {
        continue;
    }
    if i == j {
        console.write('Ending loop early; reached maximum: \{}'(j));
        break;
    }
} else {
    console.write('Maximum not reached: \{}'(j));
}
```

---

For loops are like python because they iterate over objects instead of advancing counters.

```
for i in [1, 2, 3] {
    console.write(i);
}
```

They can also include `break`, `continue`, and `else` just like `while` loops.

---

Anonymous functions are a large difference between NS and Python. There is no equivalen to the `def` keyword. Instead, all functions are created with the `function` keyword, which returns a function object. This can be assigned to a variable, or passed directly to something else without assignment.

```
add = function(a, b) {
    return a + b;
};

console.write(map(function(x) {
    return x^2;
}, [1, 2, 3, 4, 5]));
```

This particular example can be accomplished with `lambda`s in Python, but a `lambda` cannot have a for loop in it. In Python, you would have to assign a function to a variable using `def`, then pass that variable to whatever function. Here, anonymous functions can have just as many control structures as non-anonymous functions, just like JS.

Since function definitions are regular `=` declarations, a semicolon must still come at the end, unlike `if` or `for` blocks.

Both `return` and `yield` keywords work like in Python 2.

---

Class inheritance works essentially like Python. The syntax is similar to `function`. For this reason, they can be anonymous, and also require a semicolon at the end.

```
A = class(B, C) {};
```

Note: All curly brace blocks can be empty if you don't want to run any code. No need for `pass`.

---

Methods inside a class are defined using the `method` keyword. Classmethods use `classmethod`. Staticmethods can just use `function`.

Methods cannot be anonymous. They must be bound. Therefore the syntax is different from the syntax for functions.

Every time an attribute of a class is set, a visibility keyword must be used (`public`, `private`, `protected`), except for `__special__` attributes, because their visibility is fixed.

Classes must inherit from something. Classes are instances of the `class` object. `object` is the parent of all objects, similar to Python 2.

```
x_container = class() {
    
    private x_type = int;
    
    method __init__(self, x) {
        if x.__class__ == self.x_type {
            private self.x = x;
        } else {
            raise exception.valueerror("x must be of type \{}"(self.x_type));
        }
    }
    
    public method get_x(self) {
        return self.x;
    }
    
    public method set_x(self, x) {
        return self.x;
    }
    
    public classmethod get_x_type(cls) {
        return cls.x_type;
    }
    
    public f = function() {
        return "f is a static method.";
    };
    
};
```

---

`try`-`except`-`else`-`finally` follows PEP 341 logically.

Exceptions return the error message when converted to strings.

All builtin exceptions are attributes of the `exception` module. `exception.exception` is the base exception.

```
try {
    raise exception.valueerror;
} except exception.valueerror as e {
    console.write('Value error: \{}'(e));
} except exception.typeerror {
    raise;
} except exception.exception {
} else {
    console.write('No error.');
} finally {
    f.close();
}
```









---
---
---



methods and functions are both `__internal__.function` instances. The `function` keyword creates a new `__internal__.function` instance. Both the `method` and `classmethod` keywords do the same, and then bind them. `__internal__.method` instances have a `__bind__` attribute that references the object they are bound to. The `classmethod` keyword binds the method to the class. The `method` keyword leaves `__bind__` as null, allowing `__new__` to bind them when instantiated. Functions will have `.bind_asmethod` and `.bind_asclassmethod` methods that take 1 argument and set the `__bind__` attribute. All methods in a class defined with the `method` keyword will not be attributes of the class, but rather stored in `__instancemethods__`, and will be bound to the instance in `__new__`.

If the user wants to define `__new__`, they must use `classmethod`.

`tools.abc.required` can be set as a variable in a class, and those will need to be set for `__new__` to not throw an exception. Typing will also be allowed somehow.










Note: should implement `let` from JS. Useful in `for` loops.

Also implement JS's `yield*`. It fits well with unpacking notation.

Maybe make `a =+ b` equivalent to `a = b + a`.

implement read only attributes


Maybe anonymous functions can use `this` to self-reference for recursion.

Maybe `this`/`this()`/`this(0)`, then `this(1)`, then `this(2)` for nested anonymous functions (could do something similar for `self`).




For `async`-`await`, `async function` should probably not exist. In javascript, it is just a more limited version of `Promise`, and the only reason to use it is for style, and because `await` is limited to use within them. In ns, `await` will work just like in javascript, except it can be used anywhere. It will be a special function instead of a keyword, and will need parentheses. `Promise` will be renamed to `async`, but will function essentially the same way.



Definitely implement `const`. builtins will be consts.

Maybe don't make format use `\{}` because it can't be dynamic. Also, `.format` is verbose and clear, so maybe calling strings is a bad idea.
